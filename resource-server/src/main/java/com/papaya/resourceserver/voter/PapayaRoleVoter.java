package com.papaya.resourceserver.voter;

import com.papaya.resourceserver.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;

import java.util.Collection;
import java.util.List;
@Slf4j
public class PapayaRoleVoter implements AccessDecisionVoter<FilterInvocation> {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, FilterInvocation filterInvocation, Collection<ConfigAttribute> collection) {
        System.out.println(authentication.getPrincipal());
        /*User principal = (User) authentication.getPrincipal();
        User userInfo = userService.loadUserById(principal.getId());
        String requestURI = filterInvocation.getRequest().getRequestURI();
        String sessionId;
        Cookie[] cookies = filterInvocation.getHttpRequest().getCookies();
        Optional<Cookie> session = List.of(cookies).stream().filter(cookie -> !cookie.getName().toLowerCase().equals("session")).findAny();
        if (session.isPresent()) {
            sessionId = session.get().getName();
            //TODO validate session id
        } else {
            log.warn("session id is missing");
            return 0;
        }
        List<String> requiredAuthorities = getRequiredAuthorities(requestURI);
        for (String authority : requiredAuthorities){
            if (true){
                return 10;
            }
        }*/
        return 1;
    }

    private List<String> getRequiredAuthorities(String requestURI) {
        return List.of("aa", "bb");
    }
}
