package com.papaya.resourceserver.voter;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Optional;

public class PrivilegeVoter implements AccessDecisionVoter<MethodInvocation> {

   /* private @Autowired
    HttpServletRequest request;*/

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, MethodInvocation methodInvocation, Collection<ConfigAttribute> collection) {
        authentication.getPrincipal();
        Annotation[] declaredAnnotations = methodInvocation.getMethod().getDeclaredAnnotations();
        Object[] arguments = methodInvocation.getArguments();
        HttpServletRequest currentHttpRequest = getCurrentHttpRequest().get();
        return ACCESS_GRANTED;
    }

    public static Optional<HttpServletRequest> getCurrentHttpRequest() {
        return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .filter(requestAttributes -> ServletRequestAttributes.class.isAssignableFrom(requestAttributes.getClass()))
                .map(requestAttributes -> ((ServletRequestAttributes) requestAttributes))
                .map(ServletRequestAttributes::getRequest);
    }
  /*  @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, FilterInvocation filterInvocation, Collection<ConfigAttribute> collection) {
        String requestUrl = filterInvocation.getFullRequestUrl();
        HttpServletRequest request = filterInvocation.getRequest();
        return 0;
    }*/
}
