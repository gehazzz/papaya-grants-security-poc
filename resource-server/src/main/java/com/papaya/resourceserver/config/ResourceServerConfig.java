package com.papaya.resourceserver.config;

import com.papaya.resourceserver.authentication.PapayaUserAuthenticationConverter;
import com.papaya.resourceserver.filter.AuthorizationHeaderFixFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Autowired
    AuthorizationHeaderFixFilter authorizationHeaderFixFilter;

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(new PapayaUserAuthenticationConverter());
        var converter = new JwtAccessTokenConverter();
        converter.setAccessTokenConverter(accessTokenConverter);
        converter.setSigningKey("12345");
        return converter;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //addFilterBefore(authorizationHeaderFixFilter(), SecurityContextPersistenceFilter.class)
        http./*addFilterBefore(authorizationHeaderFixFilter, BasicAuthenticationFilter.class).*/authorizeRequests().anyRequest().authenticated();
        //.accessDecisionManager(affirmative());
        //http.authorizeRequests().accessDecisionManager(accessDecisionManager());
    }


   /* @Bean
    public Filter authorizationHeaderFixFilter(){
        return new AuthorizationHeaderFixFilter();
    }*/

   /* @Bean
    public AccessDecisionManager affirmative() {
        WebExpressionVoter webExpressionVoter = new WebExpressionVoter();
        webExpressionVoter.setExpressionHandler(new OAuth2WebSecurityExpressionHandler());
        List<AccessDecisionVoter<? extends Object>> decisionVoters
                = Arrays.asList(//new RoleVoter(),
                webExpressionVoter,
                new PapayaRoleVoter(),
                new AuthenticatedVoter()
                //new PrivilegeVoter()
        );

        return new UnanimousBased(decisionVoters);
    }*/
}
