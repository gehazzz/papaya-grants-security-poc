package com.papaya.resourceserver.config;

import com.papaya.resourceserver.voter.PrivilegeVoter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class GlobalMethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    @Autowired
    AccessDecisionManager accessDecisionManager;

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        return new DefaultMethodSecurityExpressionHandler();
    }

    @Override
    protected AccessDecisionManager accessDecisionManager() {
        return unanimousBased();
    }

    @Bean
    public AccessDecisionManager unanimousBased() {
        List<AccessDecisionVoter<? extends Object>> decisionVoters
                = Arrays.asList(//new RoleVoter(),
                //new WebExpressionVoter(),

                new AuthenticatedVoter(),
                new PrivilegeVoter());

        return new UnanimousBased(decisionVoters);
    }
}
