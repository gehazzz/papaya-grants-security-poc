package com.papaya.resourceserver.config;

import com.papaya.resourceserver.filter.AuthorizationHeaderFixFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.util.Arrays;
import java.util.List;

//@Order(1)
//@Configuration
//@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //addFilterBefore(authorizationHeaderFixFilter(), SecurityContextPersistenceFilter.class)
        http.addFilterBefore(authorizationHeaderFixFilter(), UsernamePasswordAuthenticationFilter.class).authorizeRequests().anyRequest().authenticated();
                //.accessDecisionManager(unanimous());
        //http.authorizeRequests().accessDecisionManager(accessDecisionManager());
    }

    @Bean
    public Filter authorizationHeaderFixFilter(){
        return new AuthorizationHeaderFixFilter();
    }

    @Bean
    public AccessDecisionManager unanimous() {
        List<AccessDecisionVoter<? extends Object>> decisionVoters
                = Arrays.asList(//new RoleVoter(),
                new WebExpressionVoter()

                //new AuthenticatedVoter(),
                //new PrivilegeVoter()
        );

        return new UnanimousBased(decisionVoters);
    }

}
