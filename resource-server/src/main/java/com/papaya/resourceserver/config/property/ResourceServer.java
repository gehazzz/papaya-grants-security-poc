package com.papaya.resourceserver.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author milfreedom
 */
@Component
@ConfigurationProperties("papaya")
public class ResourceServer {
    public String customHeaderName;
}
