package com.papaya.resourceserver.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class ProjectAuthorities {
    private List<Role> roles;
}
