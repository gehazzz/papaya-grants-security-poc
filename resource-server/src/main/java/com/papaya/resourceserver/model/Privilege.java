package com.papaya.resourceserver.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
//Can be enum - read,
public class Privilege {
    private String permission;
    private boolean isAllowed;
}
