package com.papaya.resourceserver.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class Role {
    private long id;
    private String name;
    private List<Privilege> privileges;
}
