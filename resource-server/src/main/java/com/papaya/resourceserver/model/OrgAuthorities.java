package com.papaya.resourceserver.model;


import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class OrgAuthorities {
    private List<Role> roles;
    private List<ProjectAuthorities> projectAuthoritiesList;
}
