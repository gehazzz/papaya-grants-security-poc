package com.papaya.resourceserver.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
    private long id;
    private String userName;
    private String email;
    private OrgAuthorities orgAuthorities;
}
