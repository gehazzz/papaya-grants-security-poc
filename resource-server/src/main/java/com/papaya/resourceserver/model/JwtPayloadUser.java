package com.papaya.resourceserver.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
*Represents an User from jwt payload
*  "user": {
*    "id": 1,
*    "type": "papaya",
*    "orgIds": [
*      3
*    ]
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtPayloadUser {
    private long id;
    private String type;
    private List<Long> orgIds;
}
