package com.papaya.resourceserver.Controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Secured("ROLE_ADMIN")
    @GetMapping("/hello")
    public String hello() {
        return "HELLO!";
    }
    @Secured("READ")
    @GetMapping("/org")
    public String getOrg(@RequestParam String org){
        return org;
    }
}
