package com.papaya.resourceserver.service;


import com.papaya.resourceserver.model.User;

public interface UserService {
    User loadUserById(Long id);
}
