package com.papaya.resourceserver.service;

import com.papaya.resourceserver.model.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Override
    public User loadUserById(Long id) {
        return load(id);
    }

    private User load(long id){
        Privilege privilege1 = Privilege.builder().permission("read").isAllowed(true).build();
        Privilege privilege2 = Privilege.builder().permission("create").isAllowed(false).build();
        Privilege privilege3 = Privilege.builder().permission("update").isAllowed(false).build();
        Privilege privilege4 = Privilege.builder().permission("delete").isAllowed(false).build();

        Role roleEmployee = Role.builder().id(1).name("employee").privileges(List.of(privilege1, privilege2, privilege3, privilege4)).build();

        Privilege privilege5 = Privilege.builder().permission("read").isAllowed(true).build();
        Privilege privilege6 = Privilege.builder().permission("create").isAllowed(true).build();
        Privilege privilege7 = Privilege.builder().permission("update").isAllowed(true).build();
        Privilege privilege8 = Privilege.builder().permission("delete").isAllowed(true).build();

        Role roleAdmin = Role.builder().id(1).name("admin").privileges(List.of(privilege5, privilege6, privilege7, privilege8)).build();

        ProjectAuthorities projectAuthorities = ProjectAuthorities.builder().roles(List.of(roleAdmin)).build();

        OrgAuthorities orgAuthorities = OrgAuthorities.builder()
                .roles(List.of(roleEmployee))
                .projectAuthoritiesList(List.of(projectAuthorities))
                .build();
        return User.builder().id(1).userName("employee").email("employee@papayaglobal.com").orgAuthorities(orgAuthorities).build();
    }
}
