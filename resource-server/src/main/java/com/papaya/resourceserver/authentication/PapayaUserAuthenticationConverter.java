package com.papaya.resourceserver.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.papaya.resourceserver.model.JwtPayloadUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

//TODO load user from db here (use userdetailsservice see implementation of DefaultUserAuthenticationConverter for reference)
public class PapayaUserAuthenticationConverter implements UserAuthenticationConverter/*extends DefaultUserAuthenticationConverter*/ {
    private final String USER = "user";
    private ObjectMapper jacksonObjectMapper = new ObjectMapper();
    private Collection<? extends GrantedAuthority> defaultAuthorities = null;

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication userAuthentication) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(USER)) {
            Object principal = map.get(USER);
            LinkedHashMap<String, Object> principalDetails = (LinkedHashMap<String, Object>) principal;
            JwtPayloadUser user = jacksonObjectMapper.convertValue(principalDetails, JwtPayloadUser.class);
            Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
         /*   Object principal = map.get(USERNAME);
            Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
            if (userDetailsService != null) {
                UserDetails user = userDetailsService.loadUserByUsername((String) map.get(USERNAME));
                authorities = user.getAuthorities();
                principal = user;
            }*/
            return new UsernamePasswordAuthenticationToken(principal, "N/A", authorities);
        }
        return null;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        if (!map.containsKey(AUTHORITIES)) {
            return defaultAuthorities;
        }
        Object authorities = map.get(AUTHORITIES);
        if (authorities instanceof String) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
        }
        if (authorities instanceof Collection) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils
                    .collectionToCommaDelimitedString((Collection<?>) authorities));
        }
        throw new IllegalArgumentException("Authorities must be either a String or a Collection");
    }
}
