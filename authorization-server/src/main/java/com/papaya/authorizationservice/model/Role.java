package com.papaya.authorizationservice.model;

import java.util.List;

public class Role {
    private long id;
    private String name;
    private List<Privilege> privileges;
}
